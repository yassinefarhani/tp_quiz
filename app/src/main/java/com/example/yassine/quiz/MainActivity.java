package com.example.yassine.quiz;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private CheckBox espagne;
    private CheckBox italie;
    private RadioButton staline;
    private RadioButton tataski;
    private RadioButton lenin;
    private EditText txt;
    private Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tataski = (RadioButton) findViewById(R.id.radio_button_trtaski);
        lenin = (RadioButton) findViewById(R.id.radio_button_lenine);
        staline = (RadioButton) findViewById(R.id.radio_button_staline);
        espagne = (CheckBox) findViewById(R.id.checkBox_espagne);
        italie = (CheckBox) findViewById(R.id.checkBox_italie);
        txt = (EditText) findViewById(R.id.txt_verbe);
        btn = (Button) findViewById(R.id.btnValide);
        btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View args) {
                int s=0;

                if(lenin.isChecked()) {
                    s = s + 1;
                }
                if(espagne.isChecked() && !italie.isChecked()) {
                    s = s + 1;
                }
                if(txt.getText().toString().equals("feingeons"))
                {s=s+1;}
                Toast.makeText(getApplicationContext(), "votre score est" + s + "", Toast.LENGTH_SHORT).show();


            }
        });
    }
}
